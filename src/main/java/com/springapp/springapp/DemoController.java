package com.springapp.springapp;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class DemoController {

	 ModelAndView modelAndView = new ModelAndView();
	 
	public void setModelAndView(ModelAndView modelAndView) {
		this.modelAndView = modelAndView;
	}

	@RequestMapping("/")
	public ModelAndView loadHomePage () {
	    modelAndView.setViewName("index.html");
	    return modelAndView;
	}
	
	@GetMapping("/getIntoApp")
	public ModelAndView goToDetails(@RequestParam(name = "username") String username, Model model) {
	    modelAndView.setViewName("userDetails.html");
	    model.addAttribute("username", username);
		return modelAndView;
	}
}
