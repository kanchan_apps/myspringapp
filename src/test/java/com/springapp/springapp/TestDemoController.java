package com.springapp.springapp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Collection;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

public class TestDemoController {

	@InjectMocks
	DemoController demoController;
	
	@Mock
	ModelAndView modelAndView;
	
	Model model = new Model() {
		
		@Override
		public Model mergeAttributes(Map<String, ?> attributes) {
			return null;
		}
		
		@Override
		public Object getAttribute(String attributeName) {
			return null;
		}
		
		@Override
		public boolean containsAttribute(String attributeName) {
			return false;
		}
		
		@Override
		public Map<String, Object> asMap() {
			return null;
		}
		
		@Override
		public Model addAttribute(String attributeName, Object attributeValue) {
			return null;
		}
		
		@Override
		public Model addAttribute(Object attributeValue) {
			return null;
		}
		
		@Override
		public Model addAllAttributes(Map<String, ?> attributes) {
			return null;
		}
		
		@Override
		public Model addAllAttributes(Collection<?> attributeValues) {
			return null;
		}
	};
	
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		demoController.setModelAndView(modelAndView);
		modelAndView.setViewName("userDetails.html");
	}
	
	@Test
	public void testgoToDetails() {
		model.addAttribute("username", "Demo User");
		ModelAndView result = demoController.goToDetails("TestDemo", model);
		
		assertNotNull(result);
		assertEquals(result, modelAndView);
		
	}
}
